## Project 1, Conversion of Numbers 

# Convert a number into binary, octal and hexadecimal


dec_num = int(input("Enter number: "))

print("Binary value is {} ".format(bin(dec_num)))
print("Octal value is {} ".format(oct(dec_num)))
print("Hexidecimal value is {} ".format(hex(dec_num)))

