## Tuple's ## 

#Define a tuple

prime_numbers = (2, 3, 5, 7, 11)
print(type(prime_numbers))

perfect_squares = [1, 4, 9, 16, 25, 36]
print(type(perfect_squares))

print(len(prime_numbers))
print(len(perfect_squares))

my_tuple = ("Hieee", 100, 12.47)
print(my_tuple)

print(type(my_tuple))

# Indexing tuple's 

my_tuple[0]
my_tuple[1]

print(my_tuple[0])
print(my_tuple[1])
print(my_tuple[0:2])
print(my_tuple[-1])

my_tuple.count(100)

# Difference between a tuple and list

l = ["a", "b", "c", "d", "e"]
t= ("a", "b", "c", "d", "e")
type(l)
type(t)

l[0] = "New Element"
print(l)

