## *args and *kwargs

# *args

def new_function(a,b,c):
    return a,b,c 

print(new_function("Hello", 10, "Hi"))

def new_function(*args):
    return args
    
print(new_function("Hello", 10))

def cal_per(a,b):
    return sum((a,b)) * 0.4

print(cal_per(2,3))

def cal_per(*args):
    return sum((args)) * 0.4
    
print(cal_per(9, 8, 7, 1, 3))   # with the *args method I can shove as many arguments as I want into the parenthesis. 
print(cal_per(9, 8))

def cal_per(*tom):
    return sum((tom)) * 0.4
    
print(cal_per(70,30,40))
print(cal_per())

# **kwargs

def my_function(**kwargs):
    print(kwargs)
    if "flower" in kwargs:
        print("We have {} for you!".format(kwargs["flower"]))
    else:
        print("There is no flower for you !")
        
my_function(color = "Orange", flower = "Rose")

def my_function(**asdf):           #As long as you have ** before anything that is jusst as valid as writing Kargs or even Args if its just * itself. 
    print(asdf)
    if "flower" in asdf:
        print("We have {} for you!".format(asdf["flower"]))
    else:
        print("There is no flower for you !")
        
my_function(color = "Orange", flower = "Rose")


## args and kargs together

def my_function(*args, **kwargs):
    print(args)
    print(kwargs)
    print("I would like to have {} {} ".format(args[0],kwargs["fruit"]))
    
my_function(1,2,3, color = "Green", fruit = "Apple")

