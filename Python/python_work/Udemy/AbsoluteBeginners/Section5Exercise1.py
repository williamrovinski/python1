## Else, Elif, If statement

if 3>2:
    print("Right")

if 4>7:
    print("Right")
else:
    print("Wrong")

a = 3
b = 5

if a > b:
    print("Yes, Its is right!!!")
else: 
    print("try again !!!")

drink = "Tea"

if drink == "Coffee":
    print("Humainity runs on coffee")
else:
    print("Where there is a Tea, there is a hope !!!")

drink = "Coke"

if drink == "Coke":
    print("Humainity runs on coke")
else:
    print("Where there is a Tea, there is a hope !!!")

drink = "Coke"

if drink == "Coffee":
    print("Humainity runs on coffee")
elif drink == "Water":
    print("Water is the soul of earth")
elif drink == "Coke":
    print("Coke is the new fuel for the programmer!!!")
else:
    print("Where there is a Tea, there is a hope !!!")
