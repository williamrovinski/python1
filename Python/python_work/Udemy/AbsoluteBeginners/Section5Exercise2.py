## For Loop

my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for num in my_list:
    print(num)

for num in my_list:
    if num % 2 == 0:
        print(num)

for num in my_list:
    if num % 2 == 0:
        print(num)

my_list = [1,2,3,4,5]
list_sum = 0 

for num in my_list:
    list_sum = list_sum + num 
print(list_sum)

print("\n")

my_string = "Hello World"

for letters in my_string:
    print(letters)

print("\n")

for i in "string":
    print(i)
    

my_list = [(1,2), (3,4), (5,6), (7,8)]
print(my_list)
print(len(my_list))

for tup in my_list:
    print(tup)
    
for a,b in my_list:
    print(a,b)

for a,b in my_list:
    print(a)
    
print("\n")

my_list = [(1,2,3), (4,5,6), (7,8,9), (10,11,12)]

for a,b,c in my_list:
    print(a)
    print(b, c)
    
my_dict = {"k1": 1, "k1": 2, "k1": 3}
for i in my_dict:
    print(i)
    
my_dict = {"k1": 1, "k1": 2, "k1": 3}
for i,m in my_dict:                       #any value you set in the one and two spot will become the 1st and 2nd value of any key value pair.
    print(i,m)
    

    
    
