## Project 5, A simple calculator  

def add(a, b, c, d):
    return a + b + c + d

def subtract(a, b, c, d):
    return a - b - c - d
    
def multiply(a, b, c, d):
    return a * b * c * d 
    
def divide(a, b, c, d):
    return a / b /c / d
    
print("Select Operation: ")
print("1: Add")
print("2: Subtract")
print("3: Multiply")
print("4: Divide")

choice = input("Enter choice (1/2/3/4)")

num1 = int(input("Enter first number: "))
num2 = int(input("Enter second number: "))
num3 = int(input("Enter third number: "))
num4 = int(input("Enter fourth number: "))

if choice == "1":
    print(num1, "+", num2, "+", num3, "+", num4, "=", add(num1, num2, num3, num4))
    
elif choice == "2":
    print(num1, "-", num2, "-", num3, "-", num4, "=", subtract(num1, num2, num3, num4))
    
elif choice == "3":
    print(num1, "*", num2, "*", num3, "*", num4, "=", multiply(num1, num2, num3, num4))

elif choice == "4":
    print(num1, "/", num2, "/", num3, "/", num4, "=", divide(num1, num2, num3, num4))
    
else:
    print("invalid input ")
