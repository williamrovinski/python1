## Class and Attributes

## 1) Define a class !!!

class simple():
    pass
    
my_class = simple() # Object

type(my_class)

## 2) Create a class with attributes !!!

class Person():
    def __init__(self,name,age):
        self.name = name              #Attribute, set by ---> self.xxxxxxxSomething. 
        self.age = age
        

P1 = Person("John", 27)
print(P1) 
type(P1)

print(P1.name) 
print(P1.age)  

# 3) Create a class Object attribute

class Person():
    eyes = "Blue"                   # class object attribute
    
    def __init__(self,name,age):
        self.name = name
        self.age = age
        
P1 = Person("John", 28)

print(P1.name)
print(P1.age)
print(P1.eyes)
