print("Hello, World!")

print('Hello, World!')

print("Python is 'awesome'")

print('Python is "awesome"')

print(""""Python" is 'awesome' """)

print(25)

print(2*3)

print(7+12)

print(3*4)

print(15/3)

# Python Basic's

print("I am learning python") # I am new here !!!

## Variable's

greetings = "hello"
print(greetings)

a = 25
print(a)

car = "Mercedes"
print(car)

_car = "Audi"
print(_car)

Two_Wheeler121 = "Royal Enfield"
print(Two_Wheeler121)

1car = "Honda"
print(1car)
