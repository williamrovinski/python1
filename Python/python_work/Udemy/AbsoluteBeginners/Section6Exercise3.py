## Map, filter, Lambda Expression

# Map function

def cal_square(n):
    return n*n
    
cal_square(4)

my_num = [1,2,3,4,5]

map(cal_square, my_num)

list(map(cal_square, my_num))

for items in map(cal_square, my_num):
    print(items)
    
def len_char(c):
    return len(c)
    
my_char = ["Apple", "Banana", "Mushrooms"] 

list(map(len_char, my_char))

print(list(map(len_char, my_char)))


for items in map(len_char, my_char): 
    print(items)
    
print("\n")

# Filter function

def even_num(n):
    return n % 2 == 0 

my_num = [1,2,3,4,5,6,7,8,9,10]

list(filter(even_num, my_num))

for items in filter(even_num, my_num):       #Filter method removes any item which doesn't satisfy the conditions of the function. everything from the list of my_num that isn't divisable by 2 is removed. 
    print(items)

print("\n")
#Lambda expressions

# 1) Step 1

def cal_square(n):
    return n * n
cal_square(3)
print(cal_square(3))

# 2) Step 2

def cal_square(n):return n*n
cal_square(4)
print(cal_square(4))

# 3) Step 3 

cal_square = lambda n:n*n
cal_square(5)
print(cal_square(5))

my_list = [1,2,3,4,5,6,7,8,9,10]

list(map(cal_square, my_list))

print(list(map(cal_square, my_list)))

list(map(lambda n:n*n, my_list))
print(list(map(lambda n:n*n, my_list)))
print("\n")

# Step 1

def even_num(n):
    return n % 2 == 0

print(even_num(3))

# Step 2 

def even_num(n):return n % 2 == 0
print(even_num(2))

# Step 3

even_num = lambda n:n % 2 == 0
print(even_num(5))

print("\n")

my_list = [1,2,3,4,5,6,7,8,9,10]
print(list(filter(even_num, my_list)))






    


