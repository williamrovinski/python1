## Project 4: Number guessing game !!!

import random

a = 1
b = 200

print(random.randint(a, b))
print("\n")

import random

print(random.randint(1, 100))
print("\n")

import random

guess = []

cpu_num = random = random.randint(1,100)

player_num = int(input("Enter a number between 1 and 100: "))

guess.append(player_num)

while player_num != cpu_num:
    if player_num > cpu_num:
        print("Too High !")
    else: 
        print("Too Low !")
    player_num = int(input("Enter a number between 1 and 100: "))
    
    guess.append(player_num)
    
else:
    print("Well done !!!")
    print("You have taken {} ".format(len(guess)))
    print("Here are your guesses : ")
    print(guess)
