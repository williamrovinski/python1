## Methods

class Person():

    def __init__(self, name, age):
        self.name = name
        self.age = age
        
        
    def my_function(self,number):
        print("I am {} and number is {} ".format(self.name,number))
        
P1 = Person("Maria", 28)
print(P1.age)
print(P1.name)     

print(P1.my_function(100))  


class Circle():
    pi = 3.14
    
    def __init__(self,radius):
        self.radius = radius
        self.area = self.pi * radius * radius 
        
    def Circum(self):
        return 2 * self.pi * self.radius 
        
P1 = Circle(10)

print(P1.area)
print(P1.pi)
print(P1.Circum())       

