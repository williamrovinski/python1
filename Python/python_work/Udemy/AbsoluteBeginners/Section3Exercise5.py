## String's ##

# 1) concatenation of the string's 

greeting = "Hello "
name = "Mark"
print(greeting + name)

#2) input function

greeting = "Hello "
name = input("What is your sweeeeht name?")

print(greeting + name)

# 3) Splitting of the strings 
split_string = "Hello everyone, welcome to the world of Python !!!"

print(split_string)

split_string = "Hello everyone, \nwelcome to the world of Python !!!"

print(split_string)

tab_string = "1\t2\t3\t4"
print(tab_string)

#4) indexing

color = "Orange"
len(color)

print(color[3])
print(color[0])
print(color[-1])
print(color[-2])

print(color[0:3]) #this range counts up until the 3rd option 
print(color[0:4])
print(color[-4:-1])
print(color[:3])
print(color[2:]) #Null or none is equal to zero 

# 5) String formatting 
age = str(27)                           # Changes any integer to string. 
print("I am " + age + " Year's old.")

age = 27                            
print("I am " + str(age) + " Year's old.") # Even changes the variable to a string.

print("The colors are {} {} {} ".format("Red", "Green", "Blue")) # {} those allow you to format anything as a list, string or otherwise.
print("The colors are {0} {1} {2} ".format("Red", "Green", "Blue")) # by putting 0-10 inside the {}, you call upon each/any list item denoted 0-10. 
print("The colors are {0} {0} {0} ".format("Red", "Green", "Blue"))

print("The colors are {r} {g} {b} ".format(r="Red", g="Green", b="Blue")) # The same principle applies to any character you set the value equal to. 
print("The colors are {b} {b} {b} ".format(r="Red", g="Green", b="Blue"))

print("The colors are {r} {r} {r} ".format(r="Red", g="Green", b="Blue")) 

pie = 3.14
print(pie)

print("The value of pie is {}".format(pie))                    # literally anything in .formation method. 

