## Inheritance

class Sports():
    def __init__(self):
        print("Enjoy the game")
    def health(self):                 #We can create as many methods as we so choose under the class, these methods can be called anything we want as long as we define them. 
        print("I am healthy")
    def excitement(self):
        print("I am exciting")
        
my_sport = Sports()                   #See here we create an object such as my_sport and set it equal to a class we created, in this case the class is Sports() as defined above. 
my_sport.excitement()                 #I add the method after the object and if I print it, it will return whatever that method does.
my_sport.health()


print("\n")
print(my_sport.excitement())
print("\n")

class Football(Sports):                #Football will inherit everything from Sports. 
    def __init__(self):
        Sports.__init__(self)
        print("I am football")
        
    def health(self):
        print("Playing football is good for your health")  # This will overide the previous health method. Re write a method in derived class.
       
    def world_cup(self):
        print("Every four years")
        
        
        
my_football = Football()
my_football.excitement()                 
my_football.health()
my_football.world_cup()

