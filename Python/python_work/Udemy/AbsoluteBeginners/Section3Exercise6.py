# Integers are whole numbers. 
# Floats are numbers with decimal points.

a = 14
print(a)

b = 4 
print(b)

print(a + b)
print(a - b)
print(a / b)
print(a * b)
print(a // b)      # Rounds of the numbers after decimal point 
print(a % b)

print(2 + 10 * 10 + 3)
print(5 + 20 / 4 * 2 - 7)
print(5 + ((20 / 4) * 2) - 7)
