## Dictionaries, key value pairs ##

# 1) Define a dictionary

my_dict = {"key1":"value1", "key2":"value2", "key3":"value3" }
print(my_dict)

my_dict["key1"]
print(my_dict["key1"])

fruits = {"Apple":3, "Banana":1.75, "Cherry": 2}
print(fruits)
print(fruits["Apple"])

# 2) Dictionaries with all data types

new_dict = {"k1":147, "k2": [15, 25, 35], "k3":{"Apple":3}}
print(new_dict)
print(new_dict["k2"])
print(new_dict["k2"][1])
print(new_dict["k3"])
print(new_dict["k3"]["Apple"])

d = {"students": ["a", "b", "c", "d"]}
print(d)
print(d["students"])
print(d["students"][1])
print(d["students"][1].upper())
print(d["students"][1].lower())

# 3) Add values

d = {"k1": 100, "k2": 200}
print(d)
d["k3"] = 300
print(d)

# 4) Replace Values
d["k1"] = "New One"       # Doing so will replace the value of key pair k1. 
print(d)

# 5) keys, Values, and Item's 
print(d.keys())
print(d.values())
