## File Handling

# Read file from a default folder

my_file = open("cats.txt")
print(my_file.read())
print(my_file.read())     ## If executed for a second time, it'll return blank 
print(my_file.seek(0))

my_file.readlines()
my_file.seek(0)

# Store file data into a variable
new_file = my_file.read()
print(new_file)

# Find the current directory
##pwd

# Read a file from anywhere in the computer
##my_file = open("C:\Users\William\F:\python_work\Chapter10")
print(my_file)

