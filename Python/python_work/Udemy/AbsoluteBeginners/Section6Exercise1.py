## functions 

# 1) Define a function

def my_function():
    print("Hello")
    
print(my_function)
print(my_function())

help(my_function)

def my_function():
    """
    Created by me
    input: No
    Output: Hello
    
    
    """
    print("Hello")
    
    
def greeting():
    print("Good Morning")

print("Good Morning")

# How to give anv arguement in the function?

def greeting(name):
    print("Good Morning " + name)
    
print(greeting("Will"))

def greeting(name = "Chris"):
    print("Good Morning " + name)
    
print(greeting())
print(greeting("Tom"))

my_variable = greeting("Tom")
my_variable
type(my_variable)
print(type(my_variable))

# Store data into a variable with "return"

def greeting(name = "Mark"):
    return "Hello " + name
    
print(greeting())
print(greeting("Tom"))

greeting("Tom")
a = greeting("Tom")
a
print(type(a))

#Define a function for addition of two numbers

def add(n1, n2):
    return n1 + n2
    
print(add(70, 30))

add(70, 30)

b = add(70, 30)

print(b)
print("\n")

# Q: Print the even numbers from the given list

##def even_number():
    """
    
    It's a function to print even number's
    input: A lis with numbers
    output: A list with even numbers
    
    """

##    enum = []
##    for n in 1:
##        if n % 2 == 0:
##            enum.append(n)
##        return(enum)
##print(enum)
        
##even_number([1,2,3,4,5,6,7,8,9])
##help(even_number)






