## Mostly used Built in functions in Python

# 1) Range Function

for num in range(10):
    print(num)

for num in range(3, 11):
    print(num)
    
for num in range(3, 11, 2):
    print(num)
    
my_list = [1,2,3,4,5,6,7,8,9,10]
range(10)
print(list(range(10)))
print(list(range(11)))

# 2) Enumerate 

word = "Batman"

for items in word:
    print(items)
    
for items in enumerate(word):
    print(items)
    
for a,b in enumerate(word):
    print(items)
    
for a,b in enumerate(word):
    print(a, b)
    
for a,b in enumerate(word):
    print(b)
    print(a)
    print("\n")
    
# 3) zip
list1 = [1,2,3,4,5]
list2 = ["a", "b", "c", "d", "e"]

print(zip(list1, list2))
    
for items in zip(list1, list2):
    print(items)
    
for a,b in zip(list1, list2):
    print(items)
    
for a,b in zip(list1, list2):
    print(b)

list1 = [1,2,3,4,5]
list2 = ["a", "b", "c", "d", "e"]
list3 = ["a", "b", "c"]
        
for items in zip(list1, list2, list3):   # zip method will always display the output of the smallest list in its totality 
    print(items)    
    
for a,b,c in zip(list1, list2, list3):   
    print(b,c) 
    
# 4) in

print("a" in [2,3,4])

print("a" in [2,3,4, "a"])

print("s" in "school")
print("s" in "School")

print("k1" in {"k1":100,"k2":200,"k3":300})
d = {"k1":100,"k2":200,"k3":300}

100 in d.keys()

print(100 in d.values())


# list comprehensions in python

my_string = "Spartans"
for letters in my_string:
    print(letters)
    
my_list = []
for letters in my_string:
    print(my_list.append(letters))

my_list = [letters for letters in my_string]

print(my_list)

list1 = [asdf for asdf in range(10)]               # You can place a for loop of any manner into a list and it works. In fact you can add a range till 10 if you like. 
print(list1)


