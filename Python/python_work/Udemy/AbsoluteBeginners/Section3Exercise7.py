## list's ##

my_list = ["Hello", 100, 23.47]
print(my_list)

second_list = ["one", "two", "three"]
print(second_list)

print(my_list, second_list)

# 1) concatenation

new_list = my_list + second_list
print(new_list)

# 2) Define empty list 

empty_list = []
print(empty_list)    # you can add something into empty lists

# 3) Indexing in list 

students = ["Robert", "Chris", "Katarina", "Scarlett"]
print(students)

print(students[0])
print(students[2])
print(students[-1])
print(students[0:2])
print(students[0:3])

# 4) Editing a list
students[0] = "Sam"     # Simple replace method 
print(students)

#add value
students.append("Paul")
print(students)

#remove values
students.remove("Scarlett")
print(students)

list_One = ["one", "two", "three", "four", "five"]
list_One
print(list_One)

list_One.pop()
print(list_One)

list_One.pop(1)
print(list_One)

# 5) Add list into list 

color = ["Red", "Green", "Blue", "Violet"]
print(color)
age = [21, 23, 25, 27]
print(age)

color.extend(age)        # Using the extend method can allow us to add a list into the end of the list we have already
print(color) 

# 6) Python in-build functions with the list's 
even = [2, 4, 6, 8]
odd = [1, 3, 5, 7, 9]
numbers = even + odd
print(numbers)
print(sorted(numbers))
print(len(numbers))
print(max(numbers))
print(min(numbers))
