### Live Twitter Sentiment Analysis ###

# pip install tweepy
# pip install textblob

# Create account with twitter API

## Importing the libraries ##
from textblob import TextBlob
import sys
import tweepy
import matplotlib.pyplot as plt

##Define function to calculate the percentage ##
def percentage(part, whole):
    return 100 * float(part)/float(whole)

## Connecting with the twitter API ##
    
# Importing the keys #
consumerKey = "vjoYRRU2tujUg9OOPYwVxMAYE"
consumerSecret = "rfWeLEg38HeeVQMD8GNQxxFtQBkWYV2NVUA8KkR60mC8j1aohl"
accessToken = "1045900085522051072-NzfBpytvgRRD6LjThfYJ7N8S7sRsxd"
accessTokenSecret = "jevu0hX6XQV4ES8LR0lUYSymJZ2JOegZRdQHFSvGJzehl"

# Establish the connection with API # 
auth = tweepy.OAuthHandler(consumerKey, consumerSecret)
auth.set_access_token(accessToken, accessTokenSecret)
api = tweepy.API(auth)

# Search for the term and define number of tweets # 
searchTerm = input("Enter Keyword/Tag to search about: ")
NoOfTerms = int(input("Enter how many tweets to search: "))

# Get number of tweets and searched terms together #
tweets = tweepy.Cursor(api.search, q=searchTerm).items(NoOfTerms)

## Iterate and Analyse the tweets ##

# Working of the TextBlob #
a = TextBlob("I am a bad cricket player")
a.sentiment.polarity

b = TextBlob("I am a good Cricket player")
b.sentiment.polarity

c = TextBlob("I am a cricket player")
c.sentiment.polarity

# Create variables to hold the average polarity # 

positive = 0 
negative = 0 
neutral = 0 
polarity = 0 

for tweet in tweets:
    analysis = TextBlob(tweet.text)
    polarity += analysis.sentiment.polarity
    
    if(analysis.sentiment.polarity == 0):
        neutral += 1
        
    elif(analysis.sentiment.polarity < 0.00):
        negative += 1
        
    elif(analysis.sentiment.polarity > 0.00):
        positive += 1 

# Generate the percentages using previously created function percentage #

positive = percentage(positive, NoOfTerms)
negative = percentage(negative, NoOfTerms)
neutral = percentage(neutral, NoOfTerms)
polarity = percentage(polarity, NoOfTerms)

# limit the decimal upto 2 places #
positive = format(positive, '.2f')
negative = format(negative, '.2f')
neutral = format(neutral, '.2f')

