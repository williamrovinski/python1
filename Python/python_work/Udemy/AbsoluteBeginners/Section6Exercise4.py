## Scope of the variables

# 1) Local variables 

def greetings():
    name = "Scarlett"
    print("Hello " + name)
    
print(greetings())

# 2) Enclosing functional local variable

name = "Robert"                        #Here Robert is a global variable but its not called upon. Only Chris is which is enclosed in a variable. 

def greeting():
    name = "Chris"
    def hello():
        print("Hi " + name)
    hello()
    
print(greeting())

# 3) Global variable

name = "Robert"

def greeting():
    def hello():
        print("Hi " + name)
    hello()

print(greeting())

# Use of the keyword "Global"

x = 200 

def my_function():
    global x
    x = 100
    print("The value of x is {}".format(x))


print(x)
print(my_function())
print(x)
