# -*- coding: utf-8 -*-
"""
Created on Fri Dec 21 11:27:41 2018

@author: William
"""

import re, pyperclip

# Create a regex for phone numbers
phoneRegex = re.compile(r'''
# 415-555-0000, 555-0000, (415) 555-0000, 555-0000 ext 12345, ext. 12345, x12345

(
((/d/d/d) | (\d\d\d\)))?           # area code (optional)
(/s|-)           # first separator
\d\d\d           # first 3 digits
-           # seperator
\d\d\d\d           # last 4 digits
(((ext(\.)?\s)|x)     # extension word-part (optional)
(\d{2,5}))?           # extension number-part (optional)
)

''', re.VERBOSE)

re.compile ('((\d\d\d) | (\(\d\d\d\)))?(\s|-)\d\d\d-\d\d\d\d((ext(\.)?\s|x)(\d{2,5}))?')

#TODO: Create a rehex for email addresses
emailRegex = re.compile(r'''
# some.+_thing@(\d{2,5}))?.com

[a-zA-Z0-9_.+]      # name part
@                   # @ symbol
[a-zA-Z0-9_.+]      # domain name part 

''', re.VERBOSE)

#TODO: Get the text off the clipbord
text = pyperclip.paste()

#Extract the phone/email from this list 
extractedPhone = phoneRegex.findall(text)
extractedEmail = emailRegex.findall(text)

allPhoneNumbers = []
for phoneNumber in extractedPhone:
    allPhoneNumbers.append(phoneNumber[0])

print(allPhoneNumbers)
print(extractedEmail)
#TODO: Copy the extracted email/phone to the clipboard
