# -*- coding: utf-8 -*-
"""
Created on Wed Dec 19 17:05:28 2018

@author: William
"""

def isPhoneNumber(text): # 415-555-XXXX
    if len(text) != 12:
        return False
    for i in range(0,3):
        if not text[i].isdecimal():
            return False
    if text[3] != '-':
        return False
    for i in range(4,7):
        if not text[i].isdecimal():
            return False
    if text[7] != '-':
        return False 
    for i in range(8,12):
        if not text[i].isdecimal():
            return False
    return True

message = 'Call me 415-555-1011, tomorrow at 15-555-9999'
foundNumber = False
for i in range(len(message)):
    chunk = message[i:i+12]
    if isPhoneNumber(chunk):
        print('Phone number found: ' + chunk)
        foundNumber = True
if not foundNumber:
    print('Could not find any phone numbers.')

import re # re means regular expression, if I wanted I could save the 
          #code above as its own re.py file, and simply import the function and have code thats alot less long.

message = 'Call me at 415-555-1011 tomorrow, or at 415-555-9999'

phoneNumRegex = re.compile(r'\d\d\d-\d\d\d')
mo = phoneNumRegex.search(message)
print(mo.group())

phoneNumRegex = re.compile(r'\d\d\d-\d\d\d')
mo = phoneNumRegex.findall(message)  # findall() method can get you every instance of the parameter you set forth,
                                     # in this case the parameter is the variable message. 
print(mo.group())

