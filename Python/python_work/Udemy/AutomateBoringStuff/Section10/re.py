# -*- coding: utf-8 -*-
"""
Created on Thu Dec 20 10:52:23 2018

@author: William
"""

def isPhoneNumber(text): # 415-555-XXXX
    if len(text) != 12:
        return False
    for i in range(0,3):
        if not text[i].isdecimal():
            return False
    if text[3] != '-':
        return False
    for i in range(4,7):
        if not text[i].isdecimal():
            return False
    if text[7] != '-':
        return False 
    for i in range(8,12):
        if not text[i].isdecimal():
            return False
    return True