magicians = ['alice','david','carolina']
for magician in magicians:
	print(magician)
	print(magician.title() + ", that was a great trick!")
	print("Thank you, everyone. That was a great magic show!")
	print("I can't wait to see your next trick, " + magician.title() + ".\n")
	
pizzas = ['multi-topping','mushroom','vegatable']
for pizza in pizzas:
	print(pizza)
	print(pizza.title() + ", I like mushroom pizza!" + ".\n")

print('I really enjoy eating pizza and drinking beer, but only if I earned it' + ".\n")
	
animals = ['dog', 'coyote', 'wolf']	
for animal in animals: 
	print(animal)
print("\n")

print('The dog is the best companion as well as protector, The wolf is a wild animal that is good for stalking prey and hunting in a pack, The coyote is a smaller canine and is good for taking down smaller prey in hard to reach place.')
print('All three animals are canines and can be pets.')
