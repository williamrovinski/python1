dogs = ['labrador', 'pitbull', 'dingo', 'shepard', 'mastiff', 'husky']
print("The first three items in the list are:")
for dog in dogs[:3]:
	print(dog)
	
print("Three items from the middle of the list are:")
for dog in dogs[1:4]:
	print(dog)

print("The last three items in the list are:")
for dog in dogs[3:]:
	print(dog)

friends_pizza = ['cheese', 'pepperoni', 'supreme']
my_pizza = friends_pizza [:]

friends_pizza.append('anchovies')
print(friends_pizza)
print(my_pizza)  

print("\nMy favorite pizzas are:")
for pizza in my_pizza:
	print(pizza)

print("\nMy friends favorite pizzas are:")
for pizza in friends_pizza:
	print(pizza)
