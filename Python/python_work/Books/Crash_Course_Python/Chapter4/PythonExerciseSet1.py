numbers = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21']
for number in numbers:
	print(number)
	
numbers = list(range(1,21))
print(numbers)
	
numbers = list(range(1,1000001))
print(numbers)

numbers = list(range(1,1000001))
print(min(numbers))
print(max(numbers))
print(sum(numbers))

odd_numbers = list(range(1,25,1))
for odd_number in odd_numbers:
	print(odd_numbers)	
	
threes = list(range(3,30,3))
for number in threes:
	print(number)

cubes = []
for value in range(1,11):
	cube = value**3
	cubes.append(cube)

for cube in cubes:
	print(cube)
	
squares = [value**3 for value in range(1,12)]
for cube in squares:
	print(cube)

squares = [value**1 for value in range(1,11)]
for pens in squares:
	print(pens)
	

	

