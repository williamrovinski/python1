try: 
    x = input("Give me a number: ")
    x = int(x)
    
    y = input("Give me another number: ")
    y = int(y)
    
except ValueError:
    print("Sorry, I really need a number.")

else:
    sum = x + y 
    print("The sum of " + str(x) + " and " + str(y) + " is " + str(sum) + ".")

print("\n")

print("Enter 'q' at any time to quit. \n")

while True:
    try:
        x = input("\nGive me a number: ")
        if x == 'q':
            break
        
        x = int(x)
        
        y = input("\nGive me a number: ")
        if y == 'q':
            break
            
        y = int(y)
        
    except ValueError:
        print("Sorry, I really need a number.")
        
    else: 
        sum = x + y
        print("The sum of " + str(x) + " and " + str(y) + " is " + str(sum) + ".")
        
print("\n")
    
filenames = ['cats.txt' , 'dogs.txt']

for filename in filenames:
    print("\nReading file: " + filename)
    try:
        with open(filename) as f:
            contents = f.read()
            print(contents)
    except FileNotFoundError:
        print(" Sorry, I can't find that file.")

print("\n")

filenames = ['cats.txt', 'dogs.txt']

for filename in filenames: 
    
    try: 
        with open(filename) as f:
            contents = f.read()
            
    except FileNotFoundError:
        pass
        
    else:
        print("\nReading file: " + filename)
        print(contents)
        
print("\n")

import json
    
print("I know your favorite number! Its " + str(number) + ".")


    
