filename = 'learning_python.txt'

with open(filename) as file_object:
    for line in file_object:
        print(line.rstrip())

print("\n")

filename = 'learning_python.txt'

with open(filename) as file_object:
    lines = file_object.readlines()
    
pi_string = ''
for line in lines:
    pi_string += line.rstrip()

print(pi_string)
print(len(pi_string))

print("\n")

filename = 'learning_python.txt'

with open(filename) as file_object:
    lines = file_object.readlines()
    
pi_string = ''
for line in lines:
    pi_string += line.rstrip()

birthday = input("Enter your birthday, in the form mmddyy: ")
if birthday in pi_string:
    print("Your birthday appears in the first million digits of pi!")
else:
    print("Your birthday does not aappear in the first million digits of pi.")

print("\n")

message = "I really hate that girl Stacy."
print(message.replace('Stacy', 'Becky'))


filename = 'learning_python.txt'

with open(filename) as file_object:
    for line in file_object:
        print(line.replace('Python', 'C'))
