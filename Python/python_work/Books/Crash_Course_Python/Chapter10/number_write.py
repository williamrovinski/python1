import json

numbers = [2, 3, 5, 7, 11, 13]

#At filename we choose this as the variable to store the list of numbers. you need to use the file extension .json.
filename = 'numbers.json'
#Here we refer back to the variable 
with open(filename, 'w') as f_obj:
#And Finally, we use the json.dump() function to store the list numbers in the file numbers.json, with the two arguments numbers and f_obj
    json.dump(numbers, f_obj)
