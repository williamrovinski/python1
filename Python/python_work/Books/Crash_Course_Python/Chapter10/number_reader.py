import json

#filename is the variable with a value of numbers.json, 
filename = 'numbers.json'
#We use the with open function to access our variable and its as a file object or f_obj. 
with open(filename) as f_obj:
#This creates the variable numbers which equals our json loaad function with file object as its arguement.
    numbers = json.load(f_obj)
#print the result
print(numbers)
