import json

filename = 'username.json'

with open(filename) as f_obj:
#json.load function will load up our variable usernames.
    username = json.load(f_obj)
#This will print the stored usrname or variable. 
    print("Welcome back, " + username + "!")
