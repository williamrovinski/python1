#In this file we use the the try except else block to do this.

import json
#Refactoring, This file is a little cleaner, but the function greet_user() is doing more than just greeting the user, 
#its also retrieving a stored username if one exists and prompting for a new username if one doesn't exist.
#Let's refactor greet_user() so it's not doing so many different tasks.
def greet_user():
    """Greet the user by name."""

#Load the username, if it has been stored previously.
#Otherwise, prompt for the username and store it. 

filename = 'username.json'
try:
#The with open filename is our variable being opened by the with open() function. As a file_object of f_obj.
    with open(filename) as f_obj:
#Print a message welcoming back the user in the else block. If this is the first time the user runs the program, username.json won't exist and a 
#FileNotFoundError will occur. 
        username = json.load(f_obj)
#Python will move on to the except block where we prompt the user to enter their username.
except FileNotFoundError:
    return None
else:
    return username
def greet_user():
    """Greet the user by name."""
    username = get_stored_username()
    if username:
        print("Welcome back, " + username + "!")
    else:
#We then use json.dump() to store the username and print a greeting.
    username = input("What is your name? ")
#We then use json.dump() to store the username and print a greeting.
    with open(filename, 'w') as f_obj:
        json.dump(username, f_obj)
        print("we'll remember you when you come back, " + username + "!")
else:
    print("Welcome back, " + username + "!")
    
greet_user()
