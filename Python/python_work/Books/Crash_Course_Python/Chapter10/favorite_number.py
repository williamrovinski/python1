import json

favorite_number = input("Whats's your favorite number? ")

with open('favorite_number.json', 'w') as f:
    json.dump(favorite_number, f)
    print("Thank's, I'll remember that.")

print("\n")

import json

with open('favorite_number.json') as f:
    number = json.load(f)

print("I know your favorite number! It's " + str(number) + ".")
print("\n")
