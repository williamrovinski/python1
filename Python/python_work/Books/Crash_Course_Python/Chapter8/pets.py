def describe_pet(animal_type, pet_name):
    """Display information about a pet."""
    print("\nI have a " + animal_type + ".")
    print("My " + animal_type + "'s name is " + pet_name.title() + ".")
    
describe_pet('hamster', 'harry')


def describe_pet(animal_type, pet_name, pet_type):
    """Display information about a pet."""
    print("\nI have a " + pet_name + pet_type + ".")
    print("My " + animal_type + "'s name is " + pet_name.title() + " and he is a " + pet_type + ".")
    
describe_pet('hamster', 'harry', 'mammal')

def describe_pet(animal_type, pet_name):
    """Display information about a pet."""
    print("\nI have a " + animal_type + ".")
    print("My " + animal_type + "'s name is " + pet_name.title() + ".")
    
describe_pet('hamster', 'harry')
describe_pet('dog', 'willie')

def describe_pet(animal_type, pet_name, pet_type):
    """Display information about a pet."""
    print("\nI have a " + pet_type +  "with a name of " + pet_name + ".")
    print("My " + animal_type + " is cooler than your " + pet_name.title() + " " + animal_type + " " + pet_type + ".")
    
describe_pet(pet_name='harry', animal_type='hamster', pet_type='mammal')

def describe_pet(pet_name1, pet_name2, pet_name3, animal_type='dog'):
    """Display information about a pet."""
    print("\nI have a " + animal_type + " "  "with a name of " + pet_name1 + ".")
    print("My " + animal_type + " " + pet_name2.title() + " is cooler than your " + animal_type + " " + pet_name3.title() + ".")
    
describe_pet(pet_name1='willie', pet_name2='stewart', pet_name3='victor')

def describe_pet(animal_type, pet_name):
    """Display information about a pet."""
    print("\nI have a " + animal_type + ".")
    print("My " + animal_type + "'s name is " + pet_name.title() + ".")

# A dog named Willie.    
describe_pet('willie')
describe_pet(pet_name='willie')
