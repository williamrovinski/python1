def make_shirt(shirtsize_medium, shirt_text1):
    print("I want a " + shirtsize_medium + " shirt and text " + shirt_text1 + " on it.")

make_shirt('medium', '"only god forgives"') 

print('\n')
def make_shirt(shirt_text, shirt_type='large'):
    print("I want a " + shirt_type + " shirt and text " + shirt_text + " on it.")

make_shirt('"I love Python"')

print('\n')
def make_shirt(shirt_size1, shirt_size2, shirt_size3, shirt_message, shirt_text='"I love Anime, anime is my life."'):
    print("I want two shirts " + shirt_size1 + " " + shirt_size2 + " with the text " + shirt_text + " on it.")
    print('\n')
    print("I also want a " + shirt_size3 + " shirt and it should say " + shirt_message)
    
make_shirt(shirt_size1='medium', shirt_size2='large', shirt_size3='small', shirt_message='WWE Champ') 

print('\n')
def describe_city(city2, city3, city1='Reykjavik'):
    print(city1 + " Is a city in Iceland.")
    print(city2 + " Is a city in Iceland.")
    print(city3 + " Is a city in Iceland.")

describe_city(city2='Keflavik', city3='Mosfellsbaer')
