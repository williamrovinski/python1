def show_magicians(wizard_names):
    """Display the function for the list"""
    for wizard_name in wizard_names:
        msg = "Hello, behold the great " + wizard_name + "."
        print(msg)

names = ['titus', 'shane', 'ziggy stardust', 'ron', 'lindsay']
show_magicians(names) 

def show_magicians(wizard_names, appended_wizard_names):
    """Display the function for the list"""
    while wizard_names:
        current_name = wizard_names.pop()
        
        print("The Great: " + current_name)
        appended_wizard_names.append(current_name)

def show_appended_wizard_names(appended_wizard_names):
    print("\nThe following names have been appended:")
    for appended_wizard_name in appended_wizard_names:
        print(appended_wizard_name)

wizard_names = ['titus', 'shane', 'ziggy stardust', 'ron', 'lindsay']
appended_wizard_names = []

show_magicians(wizard_names, appended_wizard_names)
show_appended_wizard_names(appended_wizard_names)

print('\n')
        
def show_magicians(magicians):
    """Print the name of each magician in the list."""
    for magician in magicians:
        print(magician)

def make_great(magicians):
    """Add 'the Great!' to each magician's name."""
    # Build a new list to hold the great musicians.
    great_magicians = []

    # Make each magician great, and add it to great_magicians.
    while magicians:
        magician = magicians.pop()
        great_magician = magician + ' the Great'
        great_magicians.append(great_magician)

    # Add the great magicians back into magicians.
    for great_magician in great_magicians:
        magicians.append(great_magician)

    return magicians

magicians = ['Harry Houdini', 'David Blaine', 'Teller']
show_magicians(magicians)

print("\nGreat magicians:")
great_magicians = make_great(magicians[:])
show_magicians(great_magicians)

print("\nOriginal magicians:")
show_magicians(magicians)
