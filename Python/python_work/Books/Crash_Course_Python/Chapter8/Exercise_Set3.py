def city_country(city_name, country_name):
    country = {'city': city_name, 'country': country_name}
    return country

citystate1 = city_country('Santiago', 'Chile')
citystate2 = city_country('Buenus Aires', 'Argentina')
citystate3 = city_country('San Paublo', 'Brazil')      
print(citystate1)  
print(citystate2) 
print(citystate3)   
print('\n')

def city_country(city_name, country_name):
    country_name = city_name + ' ' + country_name
    return country_name.title()
    
citystate1 = city_country('"Santiago,', 'Chile"')
citystate2 = city_country('"Buenus Aires,', 'Argentina"')
citystate3 = city_country('"San Paublo,', 'Brazil"') 
print(citystate1)  
print(citystate2) 
print(citystate3) 
print('\n')

def get_make_album(artist_name, album_name):
    band = artist_name + ' ' + album_name
    return band.title()
    
musician1 = get_make_album('Bono', 'Bloody Sunday')
musician2 = get_make_album('Adam Levine', 'Jager')
musician3 = get_make_album('Mick Jones', 'Great Hists')
print(musician1)  
print(musician2) 
print(musician3)

album_name = input("Please enter the tracks on the album")

print('\n')

def get_make_album(artist_name, album_name):
    band_title = artist_name + ' ' + album_name
    return band_title.title()

while True: 
    print("\nPlease Enter album name and artist:")
    print("(enter 'q' at any time to quit)")
    
    artist_name = input("Enter artist name: ")
    if artist_name == 'q':
        break
    
    album_name = input("Enter album name: ")
    if album_name == 'q':
        break
    
    make_album = get_make_album(artist_name, album_name)
    print("\nThat was a good one, " + make_album + "back in 69.")
