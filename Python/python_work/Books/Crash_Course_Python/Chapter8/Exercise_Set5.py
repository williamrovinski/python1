def make_sandwich(*contents):
    print("\nYou want a sandwich with:\n")
    for content in contents:
        print("-" + content)

make_sandwich('bacon', 'lettuce', 'tomato') 
make_sandwich('chicken salad', 'guacamole', 'tomato') 
make_sandwich('rice cake', 'lettuce', 'tofu', 'tomato', 'spinach') 
make_sandwich('egg salad', 'guacamole', 'tomato') 

print('\n')

def build_profile(first, last, **user_info):
    profile = {}
    profile['first_name'] = first
    profile['last_name'] = last
    for key, value in user_info.items():
        profile[key] = value
    return profile
    
user_profile = build_profile('William', 'Rovinski',
                             location='Springfield',
                             field='Computer Science')
print(user_profile)

print('\n')

def build_car(vehicle, model, **specifications):
    car_dict = {
        'vehicle': vehicle.title(),
        'model': model.title(), 
        }
    for specification, value in specifications.items():
        car_dict[specification] = value 
        
    return car_dict
   
my_outback = build_car('subaru', 'outback', color='blue', tow_package=True)
print(my_outback)

my_accord = build_car('honda', 'accord', year=1991, color='white', headlights='popup')
print(my_accord)


