def greet_user(username):
    """Display simple greeting"""
    print("Hello, " + username.title() + ".")
greet_user('jesse')

def greet_users(names): 
    """Print a simple greeting to each user in the list."""
    for name in names:
        msg = "Hello, " + name.title() + "!"
        print(msg)
        
usernames = ['hannah', 'ty', 'margot']
greet_users(usernames)
