# -*- coding: utf-8 -*-
"""
Chapter 12 a Ship that fires bullets
"""
import sys

import pygame

from settings2 import Settings
from ship2 import Ship
import game_functions2 as gf

def run_game():
    #Initialize game and create a screen object
    pygame.init()
    ai_settings = Settings()
    screen = pygame.display.set_mode(
            (ai_settings.screen_width, ai_settings.screen_height))
    screen = pygame.display.set_mode((1200, 800))
    pygame.display.set_caption("Alien Invasion")
    
    #Make a ship.
    ship = Ship(ai_settings, screen)
    
    #Set background color.
    bg_color = (0, 0, 225)
    
    #Start the main loop for the game
    while True:
        gf.check_events(ship)
        ship.update()
        
        gf.update_screen(ai_settings, screen, ship)

run_game()
    

