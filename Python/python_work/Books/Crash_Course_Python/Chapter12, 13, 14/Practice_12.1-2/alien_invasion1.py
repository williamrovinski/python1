# -*- coding: utf-8 -*-
"""
Chapter 12 a Ship that fires bullets
"""
import sys

import pygame

from settings1 import Settings
from ship1 import Ship
import game_functions1 as gf

def run_game():
    #Initialize game and create a screen object
    pygame.init()
    ai_settings = Settings()
    screen = pygame.display.set_mode(
            (ai_settings.screen_width, ai_settings.screen_height))
    screen = pygame.display.set_mode((1200, 800))
    pygame.display.set_caption("Alien Invasion")
    
    #Make a ship.
    ship = Ship(screen)
    
    #Set background color.
    bg_color = (0, 0, 225)
    
    #Start the main loop for the game
    while True:
        gf.check_events()
        gf.update_screen(ai_settings, screen, ship)

run_game()
    

