import sys

import pygame 

from Rules import Rules
from mister import Mister

def run_game():
    #Initialize pygame, settings, and screen object.
    pygame.init()
    ai_settings = Rules()
    screen = pygame.display.set_mode(
        (ai_settings.screen_width, ai_settings.screen_height))
    pygame.display.set_caption("Mister Cheif, battle")
    
    # Make Mister Cheif
    mister = Mister(screen)
    
    # Set the background color.
    bg_color = (0, 0, 255)
    
    # Start the main loop for the game.
    while True:
    
        # Watch for keyboard and mouse events.
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
        
        # Redraw the screen during each pass through the loop.
        screen.fill(ai_settings.bg_color)
        mister.blitme()
                
        # Make the most recently drawn screen visible.
        pygame.display.flip()

run_game()
