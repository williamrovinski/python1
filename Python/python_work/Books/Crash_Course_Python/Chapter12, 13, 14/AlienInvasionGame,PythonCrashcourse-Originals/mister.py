import pygame

class Mister():

    def __init__(self, screen):
        """Initialize the mister cheif and set its starting position."""
        self.screen = screen
        
        # Load the mister image and get its rect.
        self.image = pygame.image.load('images/Mister_Cheif.bmp')
        self.rect = self.image.get_rect()
        self.screen_rect = screen.get_rect()
        
        # Start each new cheif at the bottom center of the screen.
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom
        
    def blitme(self):
        """Draw the cheif at its current location."""
        self.screen.blit(self.image, self.rect)
