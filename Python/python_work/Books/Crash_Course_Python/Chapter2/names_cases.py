favorite_student = 'Eric'
print(favorite_student)
message = "Hello, would you like to learn some python today?"
print(message) 
print(favorite_student + message)

print(favorite_student.title())
print(favorite_student.upper())
print(favorite_student.lower())

author = "Mark Twain"
quote = "I never let my schooling interfere with my education"
print(author + quote)
