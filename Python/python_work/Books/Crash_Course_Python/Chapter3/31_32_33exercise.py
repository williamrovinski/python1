Friends = ['Rob', 'Steve', 'Mike', 'Thom', 'Christine', 'lillie']

print(Friends[0])
print(Friends[1])
print(Friends[2])
print(Friends[3])
print(Friends[4])
print(Friends[5])

message = "His name began with " + Friends[0].title() + "."
print(message)
message = "His name began with " + Friends[1].title() + "."
print(message)
message = "His name began with " + Friends[2].title() + "."
print(message)
message = "His name began with " + Friends[3].title() + "."
print(message)
message = "Her name began with " + Friends[4].title() + "."
print(message)
message = "Her name began with " + Friends[5].title() + "."
print(message)

Transportation = ['train', 'trolley', 'car', 'airplane', 'boat', 'horse', 'bicycle']

print(Transportation[0])
print(Transportation[1])
print(Transportation[2])
print(Transportation[3])
print(Transportation[4])
print(Transportation[5])
print(Transportation[6])
  
message = "Its name began with " + Transportation[0].title() + "."
print(message)
message = "Its name began with " + Transportation[1].title() + "."
print(message)
message = "Its name began with " + Transportation[2].title() + "."
print(message)
message = "Its name began with " + Transportation[3].title() + "."
print(message)
message = "Its name began with " + Transportation[4].title() + "."
print(message)
message = "Its name began with " + Transportation[5].title() + "."
print(message)
message = "Its name began with " + Transportation[6].title() + "."
print(message)
