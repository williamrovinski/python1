Places = ['Toronto', 'Sydney', 'New York', 'Seatle', 'Bangkok']
print(Places)

Places.sort()
print(Places)

print('\n')
Places = ['Toronto', 'Sydney', 'New York', 'Seatle', 'Bangkok']
print(Places)
print('\n')

Places = ['Toronto', 'Sydney', 'New York', 'Seatle', 'Bangkok']
print("Here is the original list:")
print(Places)

print("\nHere is the sorted list:")
print(sorted(Places))

print("\nHere is the original list again:")
print(Places)

print('\n')

Places = ['Toronto', 'Sydney', 'New York', 'Seatle', 'Bangkok']
Places.reverse()
print(Places)
print('\n')
Places = ['Toronto', 'Sydney', 'New York', 'Seatle', 'Bangkok']
Places.sort(reverse=True)
print(Places)

Places.sort()
print(Places)

Places.sort(reverse=True)
print(Places)

print('\n')

Guests = ['Sam', 'Caitlyn', 'Anne', 'Paul', 'Darrel', 'Mike', 'Kate', 'Benji', 'Gabbie']

print(Guests)
