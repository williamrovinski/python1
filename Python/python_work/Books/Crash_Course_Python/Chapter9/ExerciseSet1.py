class Restaurant():
    """A class representing a restaurant."""

    def __init__(self, name, cuisine_type):
        """Initialize the restaurant."""
        self.name = name.title()
        self.cuisine_type = cuisine_type

    def describe_restaurant(self):
        """Display a summary of the restaurant."""
        msg = self.name + " serves wonderful " + self.cuisine_type + "."
        print("\n" + msg)

    def open_restaurant(self):
        """Display a message that the restaurant is open."""
        msg = self.name + " is open. Come on in!"
        print("\n" + msg)

restaurant = Restaurant('the mean queen', 'pizza')
print(restaurant.name)
print(restaurant.cuisine_type)

restaurant.describe_restaurant()
restaurant.open_restaurant()

print("\n")

class Restaurant():
  
    def __init__(self, name, cuisine_type):
        self.cuisine_type = cuisine_type
        self.name = name.title()

    def describe_restaurant(self):
        msg = self.name + " this is a quality Italian restaurant which serves " + self.cuisine_type + "."
        print("\n" + msg)

    def open_restaurant(self):
        msg = self.name + " restaurant opens 2pm and closes 1am, be sure to like " + self.cuisine_type + " and carbs before you come in."
        print("\n" + msg)

diner = Restaurant('the mean queen', 'pizza')
print(diner.name)
print(diner.cuisine_type)

diner.describe_restaurant()
diner.open_restaurant()

print("\n")

diner = Restaurant('BBQ st. Louis', 'meat')
print(diner.name)
print(diner.cuisine_type)
diner.describe_restaurant()
diner.open_restaurant()
diner.describe_restaurant()
diner.open_restaurant()
diner.describe_restaurant()
diner.open_restaurant()

print("\n")

diner = Restaurant('Im Vegan!', 'vegetables')
print(diner.name)
print(diner.cuisine_type)
diner.describe_restaurant()
diner.open_restaurant()

print("\n")
    
class User():
    
    def __init__(self, first_name, last_name, gender, age, profession, skin_color, eye_color, hair_color, religion): 
        self.first_name = first_name
        self.last_name = last_name
        self.gender = gender
        self.age = age
        self.profession = profession
        self.skin_color = skin_color
        self.eye_color = eye_color
        self.hair_color = hair_color
        self.religion = religion
    
    def describe_user(self):
        msg = "Hello, " + self.first_name + " please tell me about your religion? " + self.religion  
        print("\n" + msg)

    def greet_user(self):
        msg = "Hello, " + self.first_name + " how is your day going?"
        print("\n" + msg)

John = User('John', 'Douglas', 'male', 23, 'construction', 'hawhite', 'blue', 'brown', 'who cares')
print(John.first_name)
print(John.last_name)
print(John.gender)
print(John.age)
print(John.profession)
print(John.skin_color)
print(John.eye_color)
print(John.hair_color)
print(John.religion)
John.greet_user()
John.describe_user()

Will = User('Will', 'Wallus', 'male', 23, 'construction', 'hawhite', 'blue', 'brown', 'who cares')
Will.greet_user()
Will.describe_user()

Joe = User('Joe', 'Vargas', 'male', 23, 'construction', 'hawhite', 'blue', 'brown', 'who cares')
Joe.greet_user()
Joe.describe_user()

Mike = User('Mike', 'Johnson', 'male', 23, 'construction', 'hawhite', 'blue', 'brown', 'who cares')
Mike.greet_user()
Mike.describe_user()

Mavid = User('Mavid', 'Hasselstoff', 'male', 23, 'construction', 'hawhite', 'blue', 'brown', 'who cares')
Mavid.greet_user()
Mavid.describe_user()

Alec = User('Alec', 'Faldwin', 'male', 23, 'construction', 'hawhite', 'blue', 'brown', 'who cares')
Alec.greet_user()
Alec.describe_user()

Victor = User('Victor', 'Stonewall', 'male', 23, 'construction', 'hawhite', 'blue', 'brown', 'who cares')
Victor.greet_user()
Victor.describe_user()
