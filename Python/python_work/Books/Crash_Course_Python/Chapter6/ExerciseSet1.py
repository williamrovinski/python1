Jake = { 'first_name': 'Jake', 'last_name': 'Keast', 'age': 23, 'city': 'Springfield' }
print(Jake['first_name'])
print(Jake['last_name'])
print(Jake['age'])
print(Jake['city'])

print('\n')
favorite_numbers = { 'Jack': 1, 'Rob': 2, 'Helen': 3, 'Frank': 4, 'Luke': 5 }

num = favorite_numbers['Jack']
print("Jack's favorite number is " + str(num) + ".")
num = favorite_numbers['Rob']
print("Rob's favorite number is " + str(num) + ".")
num = favorite_numbers['Helen']
print("Helen's favorite number is " + str(num) + ".")
num = favorite_numbers['Frank']
print("Frank's favorite number is " + str(num) + ".")
num = favorite_numbers['Luke']
print("Luke's favorite number is " + str(num) + ".")
print('\n')

glossary = { 'string': 'A list of words contained by quotation marks',
             'comment': 'Any word or phrase that is proceeded by a hashtag mark',
             'dictionary': 'A list of data contained by straight edged brackets',
             'loop': 'A way to repeat a series of statements in an if-elif-else chain',
             'append': 'This function allows you to place a value inside a list or array', }

word = 'string' 
print('\n' + word.title() + ': ' + glossary[word] + ".")
word = 'dictionary' 
print('\n' + word.title() + ': ' + glossary[word] + ".")
word = 'comment' 
print('\n' + word.title() + ': ' + glossary[word] + ".")
word = 'loop' 
print('\n' + word.title() + ': ' + glossary[word] + ".")
word = 'append' 
print('\n' + word.title() + ': ' + glossary[word] + ".")
