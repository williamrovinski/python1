# Make an empty list to store people in. 
people = []

# Define some people, and add them to the list. 

person = { 'first_name': 'Jake', 
           'last_name': 'Keast', 
           'age': 23, 
           'city': 'Springfield', }
people.append(person)

person = { 'first_name': 'Kelly', 
           'last_name': 'Keast', 
           'age': 18, 
           'city': 'Springfield', }
people.append(person)

person = { 'first_name': 'Matt', 
           'last_name': 'Keast', 
           'age': 13, 
           'city': 'Springfield', }
people.append(person)

# Display all of the information in the dictionary. 
for person in people: 
	name = person['first_name'].title() + "" + person['last_name'].title()
	age = str(person['age'])
	city = person['city'].title()
	
	print(name + ", of " + city + ", is " + " years old.")  


# Make an empty list to store the dictionary of pets in, so when you run the for loop it'll be contained in something. 
pets = []

#Make individual dictionaries for each pet, it will store that in the list above. 
pet = { 'animal type': 'python', 'name': 'John', 'owner': 'guido', 'weight': 43, 'eats': 'bugs', }
pets.append(pet)

pet = { 'animal type': 'chicken', 'name': 'Clarence', 'owner': 'Tiffany', 'weight': 2, 'eats': 'seeds', }
pets.append(pet) 

pet = { 'animal type': 'dog', 'name': 'Peso', 'owner': 'Eric', 'weight': 37, 'eats': 'shoes', }
pets.append(pet) 

pet = { 'animal type': 'parrot', 'name': 'Larry', 'owner': 'Nick', 'weight': 24, 'eats': 'seeds', }
pets.append(pet) 

pet = { 'animal type': 'bat', 'name': 'Dick', 'owner': 'Frank', 'weight': 33, 'eats': 'fruit', }
pets.append(pet) 

pet = { 'animal type': 'yabbie', 'name': 'Mike', 'owner': 'Cusack', 'weight': 93, 'eats': 'kelp', }
pets.append(pet) 

# Display information about each pet while running a for loop.
for pet in pets:
	print("\nHere's what I know about each pet " + pet['name'].title() + ".")
	for key, value in pet.items():
		print("\t" + key + ": " + str(value))
print('\n')
		
favorite_places = {
    'eric': ['bear mountain', 'death valley', 'tierra del fuego'],
    'erin': ['hawaii', 'iceland'],
    'ever': ['mt. verstovia', 'the playground', 'south carolina']
    }

for name, places in favorite_places.items():
    print("\n" + name.title() + " likes the following places:")
    for place in places:
        print("- " + place.title())
print('\n')
		
favorite_numbers = {
    'mandy': [42, 17],
    'micah': [42, 39, 56],
    'gus': [7, 12],
    }

for name, numbers in favorite_numbers.items():
    print("\n" + name.title() + " likes the following numbers:")
    for number in numbers:
        print("  " + str(number))
print('\n') 

#In this problem you will created a nested dictionary to go inside your dictionary
cities = {
    'santiago': {
        'country': 'chile',
        'population': 6158080,
        'nearby mountains': 'andes',
        },
    'talkeetna': {
        'country': 'alaska',
        'population': 876,
        'nearby mountains': 'alaska range',
        },
    'kathmandu': {
        'country': 'nepal',
        'population': 1003285,
        'nearby mountains': 'himilaya',
        }
    }

for city, city_info in cities.items():
    country = city_info['country'].title()
    population = city_info['population']
    mountains = city_info['nearby mountains'].title()

    print("\n" + city.title() + " is in " + country + ".")
    print("  It has a population of about " + str(population) + ".")
    print("  The " + mountains + " mountains are nearby.")

#Come back to knowing what keys, values, and items are and how they relate to a dictionary. 
