glossary = {
    'string': 'A series of characters.', 
    'comment': 'A note in a program that the Python interpreter ignores.',  
    'list': 'A collection of items in a particular order.', 
    'loop': 'Work through a collection of items, one at a time.', 
    'dictionary': "A collection of key-value pairs.",
    'key': 'The first item in a key-value pair in a dictionary.',
    'value': 'An item associated with a key in a dictionary.', 
    'conditional test': 'A comparison between two values.',
    'float': 'A numerical value with a decimal component.', 
    'boolean expression': 'An expression that evaluates to True or False.', }

for word, definition in glossary.items():
    print("\n" + word.title() + ": " + definition)
print('\n')

rivers = {
    'nile': 'egypt',
    'mississippi': 'united states',
    'fraser': 'canada',
    'kuskokwim': 'alaska',
    'yangtze': 'china', }

for river, country in rivers.items():
    print("\n" + river.title() + ": " " flows through" + country)

print("\nThe following rivers are included in this data set:")
for river in rivers.keys():
    print("- " + river.title())

print("\nThe following countries are included in this data set:")
for country in rivers.values():
    print("- " + country.title())
print('\n')
    
favorite_languages = {
    'jen': 'python',
    'sarah': 'c',
    'edward': 'ruby',
    'phil': 'python',
    }

for name, language in favorite_languages.items():
    print(name.title() + "'s favorite language is " +
        language.title() + ".")

print("\n")

coders = ['phil', 'josh', 'david', 'becca', 'sarah', 'matt', 'danielle']
for coder in coders:
    if coder in favorite_languages.keys():
        print("Thank you for taking the poll, " + coder.title() + "!")
    else:
        print(coder.title() + ", what's your favorite programming language?")
    
 
