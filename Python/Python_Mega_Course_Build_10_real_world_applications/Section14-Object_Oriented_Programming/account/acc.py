# -*- coding: utf-8 -*-
"""
account for bank 
"""


class Account:
    
    def __init__(self, filepath): ##You must use '__ __' around init otherwise you will get a type error of the class which can take no 
                                  ## no arguments.
        self.filepath=filepath
        with open(filepath, 'r') as file:
            self.balance=int(file.read())   ##Instance variable to the left
    
    def withdraw(self, amount):
        self.balance=self.balance - amount
        
    def deposit(self, amount):
        self.balance=self.balance + amount
        
    def commit(self):
        with open(self.filepath, 'w') as file:
            file.write(str(self.balance))
            
class Checking(Account):
    type="checking" 
    
    def __init__(self, filepath, fee):
        Account.__init__(self, filepath)
        self.fee=fee
        
    def transfer(self, amount):
        self.balance=self.balance - amount - self.fee
        
checking=Checking("balance.txt", 1)
checking.transfer(110)
print(checking.balance)
checking.commit()

jacks_checking=Checking("jack.txt", 1)
jacks_checking.transfer(110)
print(jacks_checking.balance)
jacks_checking.commit()

johns_checking=Checking("john.txt", 1)
johns_checking.transfer(110)
print(johns_checking.balance)
johns_checking.commit()       
#account=Account("balance.txt")
#print(account.balance)
#account.withdraw(100)
#print(account.balance)
#account.commit()
#account.deposit(500)
#print(account.balance)
#account.commit()