address = ["Flat Floor Street", "18", "New York"]
pins = {"Mike":1234, "Joe":1111, "Jack":2222}

print(address[0], address[1])

pin = int(input("Enter your pin: "))

def find_in_file(f):
    myfile = open("sample.txt")
    fruits = myfile.read()
    if f in fruits: 
        return "That fruit is in the list."
    else:
        return "No such fruit found!"

if pin in pins.values():
    fruit = input("Enter fruit: ")
    print(find_in_file(fruit))
else:
    print("Incorrect pin!")
    print("This info can be accessed only by: ")
    for key in pins.keys():
        print(key)


##Opening Files in python 
myFile = open("sample.txt")
content = myFile.read()
myFile.close()
print(content)

print('\n')

##Processing file content
file = open("fruits.txt", "r")
content = file.read()
file.close()
print(content)

myfile = open("sample2.txt")
c = myfile.read()
c.splitlines()
myfile.close()
print(c)

for item in c:
    print(item)

myfile2 = open("sample2.txt")
b = myfile2.read().split(',')
myfile2.close()
groceries = b

for v in groceries:
    print(v.title())

print('\n')

mylist = [1, 2, 3, 4, 5]
for itema in mylist:
    print(itema)
    
print('\n')
mylist = [1, 2, 3, 4, 5]
for i in mylist:
    print(i)

print('\n')
for i in (1, 2, 3):
    print(i + 1)

print('\n')
a = "Tricky"
for i in a[:3]:
    print(i)

print('\n')    
mylist = ["Trickier"]
for item in mylist:
    print(item)
    
print('\n')  
 
mylist = ["Terribly Tricky"] 
for word in mylist:
    for letter in word[-6:]:
        print(letter)