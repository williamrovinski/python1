##Custom functions multiple parameters
address = ["Flat Floor Street", "18", "New York"]
pins = {"Mike":1234, "Joe":1111, "Jack":2222}

print(address[0], address[1])

pin = int(input("Enter your pin: "))

def find_in_file(f):
    myfile = open("sample.txt")
    fruits = myfile.read()
    if f in fruits: 
        return "That fruit is in the list."
    else:
        return "No such fruit found!"

if pin in pins.values():
    fruit = input("Enter fruit: ")
    print(find_in_file(fruit))
else:
    print("Incorrect pin!")
    print("This info can be accessed only by: ")
    for key in pins.keys():
        print(key)
        
def converter(original_unit, coefficient):
    return original_unit * coefficient

def printing():
    print(100)
    
print(type(converter(2, 4)))
print(type(printing()))
print('\n')
print(printing())
print('\n')
print(converter(2, 4))
print('\n')


##Exercise18 Sum up Function
def mysum(a, b):
    return(a + b)
    
print(mysum(2, 10))

##Exercie19 Function Output
def mysum(a, b):
    return a + b
    
print(mysum(10, 20))

print('\n')

##Exercie20 Function with Default parameters
def converter(kg, coeff=2.20462):
    return kg * coeff

print(converter(69))

##Weather Function

def cel_to_fahr(celsius):
    fahrenheit = celsius * 9/5 + 32
    return fahrenheit

print(cel_to_fahr(12))

##Functions If-else
print('\n')
def string_length(answer):
    userInput = input('please enter a value')
    userInput = answer
    if userInput == 'apples':
        return len(answer)
    elif userInput == 'banannas':
        return len('banannas')
    elif userInput == 'word':
        return len('word')
    else: 
        return 'Sorry Integers dont have lengths'
    
print (string_length(10))

def string_length(mystring):
    if type(mystring) == int:
        return "Sorry, integers don't have length"
    else:
        return len(mystring)
print('\n')
     
def string_length(mystring):
    mystring = type(input())
    if type(mystring) == int:
        return "Sorry, integers don't have length"
    elif type(mystring) == float:
        return "Sorry even numbers with decimals cannot have lengths"
    elif type(mystring) == 'string': 
        return "Thats a valid value"
    else:
        return "I litrally cannot even with you right noaw, boo"

print(string_length(10))

def string_length(mystring):
     if type(mystring) == int:
         return "Sorry, integers don't have length"
     elif type(mystring) == float:
         return "Sorry, floats don't have length"
     else:
         return len(mystring)

print(string_length('OMG boo want to go to saphora'))


            
        
        
    

