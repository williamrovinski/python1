address = ["Flat Floor Street", "18", "New York"]
pins = {"Mike":1234, "Joe":1111, "Jack":2222}

print(address[0], address[1])

pin = int(input("Enter your pin: "))

def find_in_file(f):
    myfile = open("sample.txt")
    fruits = myfile.read()
    if f in fruits: 
        return "That fruit is in the list."
    else:
        return "No such fruit found!"

if pin in pins.values():
    fruit = input("Enter fruit: ")
    print(find_in_file(fruit))
else:
    print("Incorrect pin!")
    print("This info can be accessed only by: ")
    for key in pins.keys():
        print(key)
print('\n')
        
##For Loop with Conditional Block
mylist = [1, 2, 3, 4, 5] 
for integers in mylist:
    if integers > 2:
        integers = mylist[2:]
        for integer in integers:
            print(integer)

print('\n')
mylist = [1, 2, 3, 4, 5] 
for i in mylist:
    if i > 2:
        print(i)
        
print('\n')
myText = open('fruits.txt')
scribe = myText.read()
myText.close()
print(scribe)
lessen = [scribe]
for lesse in lessen:
    if lesse == 'pear\napple\norange\nmandarin\nwatermelon\npomegranate\n':
        lesse = 'pear'
        print(len(lesse))
        for blorp in lessen:
            if blorp == 'pear\napple\norange\nmandarin\nwatermelon\npomegranate\n':
                blorp = 'apple'
                print(len(blorp))
                for glorp in lessen:
                    if glorp == 'pear\napple\norange\nmandarin\nwatermelon\npomegranate\n':
                        glorp = 'orange'
                        print(len(glorp))
                        for florp in lessen:
                            if florp == 'pear\napple\norange\nmandarin\nwatermelon\npomegranate\n':
                                florp = 'mandarin'
                            print(len(florp))
                            for vlorp in lessen:
                                if vlorp == 'pear\napple\norange\nmandarin\nwatermelon\npomegranate\n':
                                    vlorp = 'watermelon'
                                print(len(vlorp))
                                for klorp in lessen:
                                    if klorp == 'pear\napple\norange\nmandarin\nwatermelon\npomegranate\n':
                                        klorp = 'pomegranate'
                                        print(len(klorp))
print('\n')

##For conditional solutions

##1. use readlines()
file = open("fruits.txt")
content = file.readlines()
content = [line.strip() for line in content]
file.close()
for i in content:
    print(len(i))

##2. use read()
myfile = open("fruits.txt")
content = myfile.read()
myfile.close()
content = content.splitlines()
for i in content:
     print(len(i))

print('\n')     
##Functions, Conditionals, and Loops
temperatures = [10, -20, 100]
 
def cel_to_fahr(celsius):
    fahrenheit = celsius * 9/5 + 32
    return fahrenheit
 
for temperature in temperatures:
    print(cel_to_fahr(temperature))
print('\n')


##Creating a text file
myFile3 = open("employees.txt", "w") ## second parameter here is how you determin what you want the file to do 
                                    ##in this case w means write the file. After its written the file will simply 
                                    ##append the txt file after each and every .close() method, 
                                    ##meaning every new line written after the method overrides and replaces the last one
                                    ##no updates.
myFile3.write("Mike")
myFile3.close()

myFile3 = open("employees.txt", "w")
myFile3.write("Mike\nJoe\nJack")
myFile3.close()
    
##Append method
myFile3 = open("employees.txt", "a")    ##'a' denotes append, so in this case after each close() method, it'll update
                                        ##The changes.
myFile3.write("\nPhil\nBill\nWill\nHill")
myFile3.close()

myFile3 = open("numbers.txt", "w")
myFile3.write('1\n2\n3')
myFile3.close()

numbers = [1, 2, 3]
file = open("numbers.txt", "w")
for i in numbers:
     file.write(str(i) + "\n")
file.close()

