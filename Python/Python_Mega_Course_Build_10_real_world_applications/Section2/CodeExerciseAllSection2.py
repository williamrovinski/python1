##Exercise3 Math Operators

x = 1
y = 2
z = 3

print(1/8)
print(2/8)
print(3/8)

print(8/x)
print(8/y)
print(8/z)

print('\n')
print(((x*y)**z)/8)

## Built-In Functions
print(dir(__builtins__))

print(int(10.5)) ## returns as integer whole number
print(round(10.5))
print(float(10)) ## returns decimal

##Exercise4 Simple Sum
a = 1.0
b = 2

print(int(a+b))

##Exercise5 Lists
blue = 1
red = 2
green = 3 

mylist = [blue, red, green]

##Exercise6 Indexing
name = "John"
print(name[2:3])
print('\n')

##Exercise7 Slicing
name = "John Smith"
print(name[2:3] + name[6:7])
print(name[2:4])

##Exercise8 More On
letters = 'abcdefghijklmnopqrstuvwxyz'
print(letters[24:25])

##Exercise9 More On
letters = 'abcdefghijklmnopqrstuvwxyz'
print(letters[23:25])

##Exercise10 Indexing(practice)
mylist = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
print(mylist[17])

##Tuple & lists, tuples are immutable yet lists and dictionaries are mutable.
l = ["a", "b", "c", "d", "e"]
t= ("a", "b", "c", "d", "e")
print(type(l))
print(type(t))

print(dir(list))

## key difference between a function and a method is the following:
## function fire off right away. However,a methos only activates after it is placed after an object.
address = ['Flat Floor Street', '18', 'New York']
print(address) 
address.append('USA') ##Here I have used the variable as an object and the method append is placed afterwards.
                      ##The method can only activated after the object 

print(address) ##simply printing the variable/now object will trigger the method to be used, USA will appear in the list
                ##If you tried printing it in the same line it won't work as far as I know.


##Quiz1 Indexing and slicing
##.remove() literally will remove anything in the parameter
##while .pop() will get rid of a range or placement of a value
numbers = [1, 2, 3, 4, 5]

numbers.remove(3)

print(numbers)
print('\n')
numbers1 = [1, 2, 3, 4, 5]

numbers1.pop(3)

print(numbers1)

print("Python is fun"[-3:][-1])
print('\n')
##Exercise 11 Append to list
mylist = ["Marry", "Jack"]
mylist.append("John")
print(mylist)

##Exercise12 Remove from list
mylist = ["Marry", "Jack", "John"]
mylist.remove("John")
print(mylist)

##Exercise13 Append from list to list
list1 = [1.2323442655, 1.4534345567, 1.023458894]
list2 = [1.9934332091]

listAlter = list1.pop(2)
list2.append(listAlter)
print(list2)

##Exercise14 Concatenate List Items(Practice)
mylist = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
blorp = mylist.pop(0)
florp = mylist.pop(24)
c = blorp + florp
print(c)

##Dictionaries --they have key value pairs
pins = {"Mike":1234, "Joe":1111, "Jack":2222}

print(pins["Mike"])

##Exercise 15 Create Dictionary
mydict = {'BadMatt':'hurts the feefees', 'GoodMatt':'makes the feefees feel good' }
print(mydict)
print('\n')

'''Let's say we have the following dictionary:

person97 = {"name":"Jack", "surname":"Smith", "age":"29"} 

##Removing pair "name":"Jack"
person97.pop("name") 
'Jack' 
print(person97) 
{'surname': 'Smith', 'age': '29'} 

##Adding new pair "name":"Jack"
person97["name"] = "Jack" 
print(person97) 
{'surname': 'Smith', 'age': '29', 'name': 'Jack'} 

##Changing an existing value
person97["age"] = 30 
print(person97) 
{'surname': 'Smith', 'age': 30, 'name': 'Jack'} 

##Mapping two lists to a dictionary:
keys = ["a", "b", "c"] 
values = [1, 2, 3] 
mydict = dict(zip(keys, values)) 
print(mydict) 
{'a': 1, 'b': 2, 'c': 3}''' 

##Inputs

user_input = int(input("Enter a number: "))
print((user_input) ** 2) 
print('\n')

##Conditionals
if 2 >= 2:
    print('yes')
else:
    print('no')
    
##Quiz2 Conditionals
if len("Hi") == 2:
    print(True)
else:
    print(False)
    
print('\n')

mynumber  = 5.0
if type(mynumber) == int:
    print("It's an integer")
elif type(mynumber) == float:
    print("It's a float")
else:
    print("It's not a number")

print('\n')    

#Custom Functions
def calculate_age(year): ##parameter to be difined later by an argument
    age = 2018 - year
    return age

print(calculate_age(1992)) ##argument goes here

def printing():
    print("Hello")


##CodingExercise 16 Create Function
def hello_world():
    return "Hello World"

print(hello_world())

print('\n')

##CodingExercise 17 Exponential Function (Practice)
def power(a):
    result = a **2
    return result

print(power(5))

##Custom functions with conditionals
address = ["Flat Floor Street", "18", "New York"]
pins = {"Mike":1234, "Joe":1111, "Jack":2222}

print(address[0], address[1])

pin = int(input("Enter your pin: "))

def find_in_file(f):
    myfile = open("sample.txt")
    fruits = myfile.read()
    if f in fruits: 
        return "That fruit is in the list."
    else:
        return "No such fruit found!"

if pin in pins.values():
    fruit = input("Enter fruit: ")
    print(find_in_file(fruit))
else:
    print("Incorrect pin!")
    print("This info can be accessed only by: ")
    for key in pins.keys():
        print(key)

print('\n')
        
##Calculate Length(practice)
def length(string): 
    return len(string)

print(length('Stop acting so obtuse!'))

print('\n')

def string_length(mystring):
    return len(mystring)

print(string_length("Hello")) 
    
print('\n')

    
