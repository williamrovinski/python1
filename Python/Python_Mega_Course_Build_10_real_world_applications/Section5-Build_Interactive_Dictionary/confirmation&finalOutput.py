import json 
from difflib import get_close_matches

data = json.load(open("data.json"))

word = input("Enter a word here: ")


def translate(w):
    w = w.lower()
    if w in data:
        return data[w]
    elif len(get_close_matches(w, data.keys())) > 0:
        yn = input("Did you mean %s instead? Enter Y if yes or N if no: " % get_close_matches(w, data.keys())[0])
        if yn == "Y":
            return data[get_close_matches(w, data.keys())[0]]
        elif yn == "N":
            return "Dind't understand the entry"
        else:
            return "Didn't work"
    else:
        return "Word doesn't exist."

output = translate(word)

if type(output) == list:
    for item in output:
        print(item)
else:
    print(item)



