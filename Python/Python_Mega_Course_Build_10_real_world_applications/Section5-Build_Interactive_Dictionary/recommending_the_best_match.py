
import json 
from difflib import get_close_matches


data = json.load(open("data.json"))

word = input("Enter a word here: ")

def translate(w):
    w = w.lower()
    if w in data:
        return data[w]
    elif len(get_close_matches(w, data.keys())) > 0:
        return "Did you mean %s instead?" % get_close_matches(w, data.keys())[0]
    else:
        return "Word doesn't exist."

print(translate(word))
print('\n')
print(type(get_close_matches))
print('\n')
    
