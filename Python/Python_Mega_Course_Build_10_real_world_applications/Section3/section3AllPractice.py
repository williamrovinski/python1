temperatures = [10,-20,-289,100]
 
def writer(temperatures):
    with open("temps.txt", 'w') as file:
        for c in temperatures:
            if c > -273.15:
                f = c* 9/5 + 32
                file.write(str(f) + "\n")
 
writer(temperatures) 

##Practice 2 
import glob2
from datetime import datetime
 
filenames = glob2.glob("*.txt")
with open(datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")+".txt", 'w') as file:
    for filename in filenames:       
        with open(filename, "r") as f:
            file.write(f.read() + "\n")


    
