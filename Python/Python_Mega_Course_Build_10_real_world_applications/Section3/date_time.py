
#Code	Meaning	Example
# %a	Weekday as locale’s abbreviated name.	Mon
# %A	Weekday as locale’s full name.	Monday
# %w	Weekday as a decimal number, where 0 is Sunday and 6 is Saturday.	1
# %d	Day of the month as a zero-padded decimal number.	30
# %-d	Day of the month as a decimal number. (Platform specific)	30
# %b	Month as locale’s abbreviated name.	Sep
# %B	Month as locale’s full name.	September
# %m	Month as a zero-padded decimal number.	09
# %-m	Month as a decimal number. (Platform specific)	9
# %y	Year without century as a zero-padded decimal number.	13
# %Y	Year with century as a decimal number.	2013
# %H	Hour (24-hour clock) as a zero-padded decimal number.	07
# %-H	Hour (24-hour clock) as a decimal number. (Platform specific)	7
# %I	Hour (12-hour clock) as a zero-padded decimal number.	07
# %-I	Hour (12-hour clock) as a decimal number. (Platform specific)	7
# %p	Locale’s equivalent of either AM or PM.	AM
# %M	Minute as a zero-padded decimal number.	06
# %-M	Minute as a decimal number. (Platform specific)	6
# %S	Second as a zero-padded decimal number.	05
# %-S	Second as a decimal number. (Platform specific)	5
# %f	Microsecond as a decimal number, zero-padded on the left.	000000
# %z	UTC offset in the form +HHMM or -HHMM (empty string if the the object is naive).	
# %Z	Time zone name (empty string if the object is naive).	
# %j	Day of the year as a zero-padded decimal number.	273
# %-j	Day of the year as a decimal number. (Platform specific)	273
# %U	

# Week number of the year (Sunday as the first day of the week) as a zero padded decimal number. All days in a new year preceding the first Sunday are considered to be in week 0.	39
# %W	

# Week number of the year (Monday as the first day of the week) as a decimal number. All days in a new year preceding the first Monday are considered to be in week 0.	39
# %c	Locale’s appropriate date and time representation.	Mon Sep 30 07:06:05 2013
# %x	Locale’s appropriate date representation.	09/30/13
# %X	Locale’s appropriate time representation.	07:06:05
# %%	A literal '%' character.	%


# Note: Examples are based on datetime.datetime(2013, 9, 30, 7, 6, 5)

# Source: http://strftime.org/

## Notes from the cmd kernel for datetime  

from datetime import datetime

delta = datetime.bnow() - datetime(1900, 12, 31)
Traceback (most recent call last):

  File "<ipython-input-2-c02529137cec>", line 1, in <module>
    delta = datetime.bnow() - datetime(1900, 12, 31)

AttributeError: type object 'datetime.datetime' has no attribute 'bnow'




delta = datetime.now() - datetime(1900, 12, 31)

delta.fays
Traceback (most recent call last):

  File "<ipython-input-4-704b1a8a2758>", line 1, in <module>
    delta.fays

AttributeError: 'datetime.timedelta' object has no attribute 'fays'




delta.days
Out[5]: 43280

delta.seconds
Out[6]: 43377

datetime.datetime(2018, 3, 2, 20, 5, 22)
Traceback (most recent call last):

  File "<ipython-input-7-c1d5d7ade9fc>", line 1, in <module>
    datetime.datetime(2018, 3, 2, 20, 5, 22)

AttributeError: type object 'datetime.datetime' has no attribute 'datetime'




datetime.now()
Out[8]: datetime.datetime(2019, 6, 30, 12, 7, 20, 742005)

dir(datetime)
Out[9]: 
['__add__',
 '__class__',
 '__delattr__',
 '__dir__',
 '__doc__',
 '__eq__',
 '__format__',
 '__ge__',
 '__getattribute__',
 '__gt__',
 '__hash__',
 '__init__',
 '__init_subclass__',
 '__le__',
 '__lt__',
 '__ne__',
 '__new__',
 '__radd__',
 '__reduce__',
 '__reduce_ex__',
 '__repr__',
 '__rsub__',
 '__setattr__',
 '__sizeof__',
 '__str__',
 '__sub__',
 '__subclasshook__',
 'astimezone',
 'combine',
 'ctime',
 'date',
 'day',
 'dst',
 'fold',
 'fromisoformat',
 'fromordinal',
 'fromtimestamp',
 'hour',
 'isocalendar',
 'isoformat',
 'isoweekday',
 'max',
 'microsecond',
 'min',
 'minute',
 'month',
 'now',
 'replace',
 'resolution',
 'second',
 'strftime',
 'strptime',
 'time',
 'timestamp',
 'timetuple',
 'timetz',
 'today',
 'toordinal',
 'tzinfo',
 'tzname',
 'utcfromtimestamp',
 'utcnow',
 'utcoffset',
 'utctimetuple',
 'weekday',
 'year']

now = datetime.now()

now
Out[11]: datetime.datetime(2019, 6, 30, 12, 9, 28, 968505)

datetime.now()
Out[12]: datetime.datetime(2019, 6, 30, 12, 10, 14, 724556)

then = datetime(1900, 12, 31, 20, 12, 59, 83845)

then
Out[14]: datetime.datetime(1900, 12, 31, 20, 12, 59, 83845)

now
Out[15]: datetime.datetime(2019, 6, 30, 12, 9, 28, 968505)

whenever = datetime.strptime("2017-12-31", "%Y-%m-%d")

whenever
Out[17]: datetime.datetime(2017, 12, 31, 0, 0)

whenever1 - datetime.strptime("2017:12:31:20:59", "%Y:%m:%d:%H:%M")
Traceback (most recent call last):

  File "<ipython-input-18-7f28893a1056>", line 1, in <module>
    whenever1 - datetime.strptime("2017:12:31:20:59", "%Y:%m:%d:%H:%M")

NameError: name 'whenever1' is not defined




whenever1 = datetime.strptime("2017:12:31:20:59", "%Y:%m:%d:%H:%M")

whenever1
Out[20]: datetime.datetime(2017, 12, 31, 20, 59)

whenever1.strftime("%Y")
Out[21]: '2017'

whenever1.strftime("%Y-%m-%d %H:%M")
Out[22]: '2017-12-31 20:59'

dir(datetime)
Out[23]: 
['__add__',
 '__class__',
 '__delattr__',
 '__dir__',
 '__doc__',
 '__eq__',
 '__format__',
 '__ge__',
 '__getattribute__',
 '__gt__',
 '__hash__',
 '__init__',
 '__init_subclass__',
 '__le__',
 '__lt__',
 '__ne__',
 '__new__',
 '__radd__',
 '__reduce__',
 '__reduce_ex__',
 '__repr__',
 '__rsub__',
 '__setattr__',
 '__sizeof__',
 '__str__',
 '__sub__',
 '__subclasshook__',
 'astimezone',
 'combine',
 'ctime',
 'date',
 'day',
 'dst',
 'fold',
 'fromisoformat',
 'fromordinal',
 'fromtimestamp',
 'hour',
 'isocalendar',
 'isoformat',
 'isoweekday',
 'max',
 'microsecond',
 'min',
 'minute',
 'month',
 'now',
 'replace',
 'resolution',
 'second',
 'strftime',
 'strptime',
 'time',
 'timestamp',
 'timetuple',
 'timetz',
 'today',
 'toordinal',
 'tzinfo',
 'tzname',
 'utcfromtimestamp',
 'utcnow',
 'utcoffset',
 'utctimetuple',
 'weekday',
 'year']

whenever1.month
Out[24]: 12

whenever1.year
Out[25]: 2017

whenever1.day
Out[26]: 31