# -*- coding: utf-8 -*-
"""
backend file for the database

"""

import sqlite3

def connect():
    conn=sqlite3.connect("books.db")
    cur=conn.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS book (id INTEGER PRIMARY KEY, title text, author text, year integer, isbn integer)")
    conn.commit()
    conn.close()
    
def insert(title,author,year,isbn):
    conn=sqlite3.connect("books.db")
    cur=conn.cursor()
    cur.execute("INSERT INTO book VALUES (NULL,?,?,?,?)",(title,author,year,isbn))
    conn.commit()
    conn.close()
    
def view():
    conn=sqlite3.connect("books.db")
    cur=conn.cursor()
    cur.execute("SELECT * FROM book")
    rows=cur.fetchall()
    conn.close()
    return rows

def search(title="",author="",year="",isbn=""):
    conn=sqlite3.connect("books.db")
    cur=conn.cursor()
    cur.execute("SELECT * FROM book WHERE title=? OR author=? OR year=? OR isbn=?", (title,author,year,isbn))
    rows=cur.fetchall()
    conn.close()
    return rows

def delete(id):
    conn=sqlite3.connect("books.db")
    cur=conn.cursor()
    cur.execute("DELETE FROM book WHERE id=?",(id,)) ## id, must be written as such to make 'id' useable it must be a tuple
                                                        ## tuples are immutable and thus can be read as such. I bet in sqlite
                                                        ## it won't allow integers to be read, but a tuple thats an immutable list 
                                                        ## would surely be passed in or accepted.
    conn.commit()
    conn.close()
    
def update(id,title,author,year,isbn):
    conn=sqlite3.connect("books.db")
    cur=conn.cursor()
    cur.execute("Update book SET title=?, author=?, year=?, isbn=? WHERE id=?", (title,author,year,isbn,id)) ##tuples need to be 
                                                                                                            ##placed at the end
                                                                                                            ##in order to work correctly
                                                                                                            ##order really matters
    conn.commit()
    conn.close()
    
    
    
connect()
##insert("The sea", "John Tablet",1918,913123132)
##insert("The Earth", "John Smith",1918,913123132)
##insert("The Sun", "John Smith",1918,913123132)
##update(4,"The moon","John Smooth",1917,99999)
##delete(3)
print(view())
print(search(author="John Smooth"))
