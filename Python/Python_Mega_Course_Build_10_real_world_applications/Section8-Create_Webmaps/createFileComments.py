import folium

##2.The Basemap
map = folium.Map(location=[39.952950, -75.332207], zoom_start=6)

map.save("Map1.html")

##3.Adding points
#import folium
#map = folium.Map(location=[39.952950, -75.332207], zoom_start=6)

#map.save("Map1.html")

##4.Adding Points
#import folium
#map = folium.Map(location=[39.952950, -75.332207], zoom_start=6, tiles="Mapbox Bright")

#map.save("Map1.html")

##5. Adding Multiple Points
##import folium
##map = folium.Map(location=[39.952950, -75.332207], zoom_start=6, tiles="Mapbox Bright")

##fg = folium.FeatureGroup(name="My Map")
##fg.add_child(folium.Marker(location=[39.95, -75.33], popup="Hi I am a Marker", icon=folium.Icon(color='green')))

##map.add_child(fg)

##map.save("Map1.html")

##6. Adding multiple points
##import folium

##map = folium.Map(location=[39.952950, -75.332207], zoom_start=6, tiles="Mapbox Bright")
##fg = folium.FeatureGroup(name="My Map")

##for coordinates in [[39.95, -75.33],[40.00, -77.33]]:
##    fg.add_child(folium.Marker(location=coordinates, popup="Hi I am a Marker", icon=folium.Icon(color='green')))

##map.add_child(fg)

##map.save("Map1.html")

##7. Adding points from files
#import folium
#import pandas

#data = pandas.read_csv("Volcanoes.txt")
#lon = list(data["LON"])
#lat = list(data["LAT"])

#map = folium.Map(location=[39.952950, -75.332207], zoom_start=6, tiles="Mapbox Bright")
#fg = folium.FeatureGroup(name="My Map")

#for lt, ln in zip(lat, lon):
#    fg.add_child(folium.Marker(location=[lt, ln], popup="Hi I am a Marker", icon=folium.Icon(color='green')))

#map.add_child(fg)

#map.save("Map1.html")

##8.Color Points
#import folium
#import pandas

#data = pandas.read_csv("Volcanoes.txt")
#lon = list(data["LON"])
#lat = list(data["LAT"])
#elev = list(data["ELEV"])

#def color_producer(elevation):
#    if elevation > 1000:
#        return 'green'
#    elif elevation >= 1000:
#        return 'orange'
#    elif elevation < 3000:
#        return 'orange'
#    else:
#        return 'red'

#map = folium.Map(location=[38.58, -99.09], zoom_start=6, tiles="Mapbox Bright")
#fg = folium.FeatureGroup(name="My Map")

#for lt, ln, el in zip(lat, lon, elev):
#    fg.add_child(folium.CircleMarker(location=[lt, ln], popup=str(el)+"m", icon=folium.Icon(color=color_producer(el))))

#map.add_child(fg)

#map.save("Map1.html")

##9.Adding more style
#import folium
#import pandas

#data = pandas.read_csv("Volcanoes.txt")
#lon = list(data["LON"])
#lat = list(data["LAT"])
#elev = list(data["ELEV"])

#def color_producer(elevation):
#    if elevation > 1000:
#        return 'green'
#    elif elevation >= 1000:
#        return 'orange'
#    elif elevation < 3000:
#        return 'orange'
#    else:
#        return 'red'

#map = folium.Map(location=[38.58, -99.09], zoom_start=6, tiles="Mapbox Bright")
#fg = folium.FeatureGroup(name="My Map")

#for lt, ln, el in zip(lat, lon, elev):
#    fg.add_child(folium.CircleMarker(location=[lt, ln], radius=6, popup=str(el)+"m", 
#    fill_color=color_producer(el), color = 'grey', fill_opacity=0.7))

#map.add_child(fg)

#map.save("Map1.html")